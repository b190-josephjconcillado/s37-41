const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// routes
const userRoutes = require("./routes/userRoutes.js");
const courseRoutes = require("./routes/courseRoutes.js");

const app = express();
// const port = 3000;

// MongoDB Connection
mongoose.connect("mongodb+srv://josephjconcillado:dBOpsesYrUx0H4ib@wdc028-course-booking.i1zei0q.mongodb.net/b190-course-booking?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", ()=> console.log("We're connected to the database"));

app.use(cors());

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users",userRoutes);
app.use("/courses", courseRoutes);

// Server Running Confirmation
app.listen(process.env.PORT||4000,()=>{
	console.log(`API now online at port ${process.env.PORT||4000}`)
});
// app.listen(port, ()=> console.log(`Server running at port: ${port}`));