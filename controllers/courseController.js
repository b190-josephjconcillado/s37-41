const Course = require("../models/course.js");



module.exports.addCourse = (reqBody) => {
    return Course.find({name: reqBody.name}).then((result,error) => {
        if(error) {
            console.log(error);
            return error;
        } else {
            if(result.length > 0) {
                return "Course is already added."
            } else {
                let newCourse = new Course ({
                    name: reqBody.name,
                    description: reqBody.description,
                    price: reqBody.price
                });
                return newCourse.save().then((course,error) => {
                    if(error) {
                        return false;
                    } else {
                        return true;
                    }
                })
            }
        };
    });
};

module.exports.getAllCourses = () => {
    return Course.find({}).then(result => {
        return result;
    });
};

module.exports.getAllActive = () => {
    return Course.find({isActive: true}).then(result => {
       return result;
    });
};

module.exports.getCourse = (courseId) => {
    return Course.findById(courseId).then(result => {
        return result;
     });
};

module.exports.updateCourse = (courseId,reqBody) => {
    let upate = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.prce
    };
	return Course.findByIdAndUpdate(courseId,upate).then((result, error) => {
		if(error) {
			console.log(error);
			return false;
		} else {
			return true;
		};
	});
};

module.exports.archiveCourse = (courseId) => {
    let archive = {
        isActive: false
    };
	return Course.findByIdAndUpdate(courseId,archive).then((result, error) => {
		if(error) {
			console.log(error);
			return false;
		} else {
			return true;
		};
	});
};