const User = require("../models/user.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Course = require("../models/course.js");

module.exports.checkEmailExists = (reqBody) => {
  return User.find({ email: reqBody.email }).then((result, error) => {
    if (error) {
      console.log(error);
      return error;
    } else {
      if (result.length > 0) {
        return true;
      } else {
        return false;
      }
    }
  });
};
module.exports.registerUser = (reqBody) => {
  return User.find({ email: reqBody.email }).then((result, error) => {
    if (error) {
      console.log(error);
      return error;
    } else {
      if (result.length > 0) {
        return false;
      } else {
        let newUser = new User({
          firstName: reqBody.firstName,
          lastName: reqBody.lastName,
          email: reqBody.email,
          mobileNo: reqBody.mobileNo,
          password: bcrypt.hashSync(reqBody.password, 10),
        });
        // if(reqBody. !== null){
        //     newUser.isAdmin = reqBody.isAdmin;
        // }
        return newUser.save().then((user, error) => {
          if (error) {
            return false;
          } else {
            return true;
          }
        });
      }
    }
  });
};

module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    if (result === null) {
      return false;
    } else {
      const isPasswordCorrect = bcrypt.compareSync(
        reqBody.password,
        result.password
      );
      if (isPasswordCorrect) {
        console.log(result);
        return {
          access: auth.createAccessToken(result),
        };
      } else {
        return false;
      }
    }
  });
};

// module.exports.getProfile = (reqBody) => {
//     return User.findById({_id: reqBody.id}).then(result => {
//         if(result === null){
//             return false;
//         } else {
//             result.password = "";
//             return result;
//         };
//     });
// };

module.exports.getProfile = (reqBody) => {
  return User.findById(reqBody.id).then((result) => {
    if (result === null) {
      return false;
    } else {
      result.password = "";
      return result;
    }
  });
};

module.exports.getAllProfile = () => {
  return User.find({}).then((result) => {
    if (result === null) {
      return false;
    } else {
      if (result.length > 0) {
        for (i = 0; i < result.length; i++) {
          result[i].password = "";
        }
      }
      return result;
    }
  });
};

module.exports.updateUserAccess = (id, reqBody) => {
  let admin = {
    isAdmin: reqBody.isAdmin,
  };
  return User.findByIdAndUpdate(id, admin).then((result, error) => {
    if (error) {
      console.log(error);
      return false;
    } else {
      return true;
    }
  });
};

// enroll a user to a class
module.exports.enroll = async (data) => {
  let isUserUpdated = await User.findById(data.userId).then((user) => {
    if (user.enrollments.length > 0) {
      for (i = 0; i < user.enrollments.length; i++) {
        if (data.courseId == user.enrollments[i].courseId) {
          console.log(false);
          return false;
        }
      }
      user.enrollments.push({ courseId: data.courseId });
      return user.save().then((user, error) => {
        if (error) {
          console.log(error);
          return false;
        } else {
          return true;
        }
      });
    } else {
      user.enrollments.push({ courseId: data.courseId });
      return user.save().then((user, error) => {
        if (error) {
          console.log(error);
          return false;
        } else {
          return true;
        }
      });
    }
    // user.enrollments.push({courseId: data.courseId});

    // return user.save().then((user,error) => {
    //     if(error) {
    //         console.log(error);
    //         return false;
    //     } else {
    //         return true;
    //     }
    // })
  });

  let isCourseUpdated = await Course.findById(data.courseId).then((course) => {
    if (course.enrollees.length > 0) {
      for (i = 0; i < course.enrollees.length; i++) {
        if (data.userId == course.enrollees[i].userId) {
          console.log(false);
          return false;
        }
      }
      course.enrollees.push({ userId: data.userId });
      return course.save().then((course, error) => {
        if (error) {
          console.log(error);
          return false;
        } else {
          return true;
        }
      });
    } else {
      course.enrollees.push({ userId: data.userId });
      return course.save().then((course, error) => {
        if (error) {
          console.log(error);
          return false;
        } else {
          return true;
        }
      });
    }

    // course.enrollees.push({userId: data.userId});
    // return course.save().then((course,error) => {
    //     if(error) {
    //         console.log(error);
    //         return false;
    //     } else {
    //         return true;
    //     }
    // })
  });
  if (isUserUpdated && isCourseUpdated) {
    return true;
  } else {
    return false;
  }
};
