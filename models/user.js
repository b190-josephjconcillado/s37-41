const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    firstName:{
        type: String,
        required: [true,"First name is required"]
    },
    lastName: {
        type: String,
        required:[true,"Last name is required"]
    },
    email:{
        type: String,
        required:[true,"email address is required"]
    },
    password:{
        type: String,
        required:[true,"Passwor is required"]
    },
    isAdmin:{
        type: Boolean,
        default: false
    },
    mobileNo:{
        type: Number,
        required:[true,"Mobile number is required"]
    },
    enrollments:[{
        courseId: {
            type: String,
            required: [true,"courseId is required"]
        },
        enrolledOn:{
            type: Date,
            default:new Date()
        },
        status:{
            type: String,
            default: "Enrolled"
        }
    }]
});

module.exports = mongoose.model("User",userSchema);