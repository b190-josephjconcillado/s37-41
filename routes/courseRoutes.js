const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController.js");
const auth = require("../auth.js")

// router.post("/", (req,res) => {

//     courseController.addCourse(req.body).then(resultFromController => res.send(
//         resultFromController));
// });

// Activity code s39
router.post("/",auth.verify, (req,res)=>{
    const userData = auth.decode(req.headers.authorization);
    if(userData.isAdmin) {
        courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
    } else {
        res.send(false);
    }
});

router.get("/all",(req,res)=>{
    courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});
router.get("/",(req,res) => {
    courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});

router.get('/:courseId',(req,res)=> {
    courseController.getCourse(req.params.courseId).then(resultFromController => res.send(resultFromController));
});

router.put('/:courseId',auth.verify, (req,res)=> {
    courseController.updateCourse(req.params.courseId,req.body).then(resultFromController => res.send(resultFromController));
});

router.put('/:courseId/archive',auth.verify, (req,res)=> {
    const userData = auth.decode(req.headers.authorization);
    if(userData.isAdmin) {
        courseController.archiveCourse(req.params.courseId).then(resultFromController => res.send(resultFromController));
    } else {
        res.send(false);
    }
});

module.exports = router;