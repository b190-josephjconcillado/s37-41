const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js")
const auth = require("../auth.js");

router.post("/checkEmail", (req,res) => {
    userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/register", (req,res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});



router.post("/login",(req,res)=>{
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// router.post("/details",(req,res)=>{
//     userController.getProfile(req.body).then(resultFromController => res.send(resultFromController));
// });

router.get("/details",auth.verify, (req,res)=>{
    const userData = auth.decode(req.headers.authorization);
    console.log(userData);
    userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController));
});

router.get("/all_details",auth.verify, (req,res)=>{
    const userData = auth.decode(req.headers.authorization);
    if(userData.isAdmin){
        userController.getAllProfile().then(resultFromController => res.send(resultFromController));
    } else {
        res.send(false);
    }
});

router.put("/:id/updateUserAccess",auth.verify,(req,res) => {
    const userData = auth.decode(req.headers.authorization);
    if(userData.isAdmin){
        userController.updateUserAccess(req.params.id,req.body).then(resultFromController => res.send(resultFromController));
    } else {
        res.send(false);
    }
});

// router.post("/enroll",(req,res) => {
//     let data = {
//         userId: req.body.userId,
//         courseId: req.body.courseId
//     }
//     userController.enroll(data).then(resultFromController => res.send(resultFromController));

// });

router.post("/enroll",auth.verify, (req,res) => {
    authData = auth.decode(req.headers.authorization);
    let data = {
        userId: authData.id,
        courseId: req.body.courseId,
        isAdmin: authData.isAdmin
    }
    if(!data.isAdmin){
        userController.enroll(data).then(resultFromController => res.send(resultFromController));
    } else {
        res.send(false);
    }

});

module.exports = router;